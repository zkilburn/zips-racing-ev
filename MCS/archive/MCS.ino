//Motor controller controller
//  systems take in throttle and brake signals from rs 485 bus
//    and repeat it out through I2C DAC to the motor controller
//  
//    Features 
//     - -  2 Temperature inputs for each motor (ADC1, ADC2)
//     - - I2C device communications for DAC control (1010110)  (0x56)
//                                       ADC control(1101100/1101000) (0x6C/0x68)
//                           -- AOUT1 (CH2 (1101100)) (Vout-c (1010110))
//                           -- AOUT2 (CH1 (1101100)) (Vout-a (1010110))
//                           -- AOUT3 (CH1 (1101000)) (Vout-d (1010110))
//                           -- AOUT4 (CH2 (1101000)) (Vout-b (1010110))
//     - -                               
//     - - 485 bus communications (Serial)
//     - - 485 I/O direction control select (INT 0) D2
//   
//     - - Interrupt pin 1 senses the halt condition (INT0)
//     - - we can trigger a halt by pulling pin D4 high
//
//     - - Relay control on D8
//
//     - - LED control on pin 12

#include <Wire.h>

//Pin definitions
#define Temp1 A1
#define Temp2 A2
#define WDT A5
#define RS485_direction 2
#define haltPin 4
#define Relay 8
#define LED 12

#define TALK HIGH
#define LISTEN LOW
#define DAC_a 0x00
#define DAC_b 0x01
#define DAC_c 0x02
#define DAC_d 0x03

#define DAC_in 0x00
#define DAC_update 0x01
#define DAC_in_update_all 0x02
#define DAC_write_and_update 0x03
#define DAC_power_down_up 0x04
#define DAC_load_clear_code 0x05
#define DAC_load_LDAC 0x06

#define ADC_1 0x6C
#define ADC_2 0x68

#define ADC_channel_1 0x80
#define ADC_channel_2 0xA0

uint8_t input[50];
int count, receiveVolts;
uint8_t lowb, highb;
//Variable declarations
void writeDAC(int voltage, uint8_t command, uint8_t channel){
  lowb =(voltage & 0x000F) << 4;
  highb=(voltage & 0x0FF0) >> 4;
  Wire.beginTransmission(0x56); // transmit to DAC  
  Wire.write(((uint8_t)(command<<4)&0xF0) + channel);              // sends    (c3,c2,c1,c0,A3,A2,A1,A0) 0001  
  Wire.write(highb);            // sends    (high byte)  xxxx xxxx
  Wire.write(lowb);              // sends    (low byte) xxxx 0000
  Wire.endTransmission();      // stop transmitting
}


int readADC(bool ADC1, bool channel1){
  count=0;
  receiveVolts=0;
  uint8_t addr;
  uint8_t command;

  if(ADC1)
    addr=ADC_1;
  else  
    addr=ADC_2;

  if(channel1)
    command=ADC_channel_1;
  else
    command=ADC_channel_2;

  Wire.requestFrom(addr, command);    // request 6 bytes from slave device #2
  while(Wire.available()<3);

  receiveVolts =  Wire.read(); // receive a byte as character
  receiveVolts=receiveVolts<<8;
  receiveVolts += Wire.read();

  return receiveVolts; 
}

void setup() {
  // put your setup code here, to run once:
  pinMode(Temp1, INPUT);
  pinMode(Temp2, INPUT);
  pinMode(RS485_direction,OUTPUT);
  pinMode(haltPin,OUTPUT);
  pinMode(Relay,OUTPUT);
  pinMode(LED,OUTPUT);
  pinMode(WDT,OUTPUT);
  digitalWrite(RS485_direction,LOW);
  Serial.begin(38400);
  Wire.begin();
}

void loop() {
  // put your main code here, to run repeatedly

  //  writeDAC(500000,DAC_write_and_update,DAC_a);
  //  writeDAC(500000,DAC_write_and_update,DAC_b);
  //  writeDAC(500000,DAC_write_and_update,DAC_c);
  //  writeDAC(500000,DAC_write_and_update,DAC_d);
  //  digitalWrite(LED,HIGH);
  //  delay(readADC(true,false));
  //  digitalWrite(LED,LOW);  
  //  delay(readADC(true,false));
  //  

  while(Serial.available()){
    input[count]=Serial.read(); 
    count++;
    delay(5);
  }
  if (count>3){
    digitalWrite(RS485_direction,TALK);
    delay(5);
    for(int i=0;i<count;i++){
      Serial.write(input[i]); 
    }
    delay(5);
    digitalWrite(RS485_direction,LISTEN);
    count=0;
  }

}


/*
while (Wire.available())   // slave may send less than requested
 {
 receiveVolts=   Wire.read(); // receive a byte as character
 //Serial.print(receiveVolts);         // print the character
 } 
 */

