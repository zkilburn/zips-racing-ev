//communications

void initCommOut() {
  //Start byte
  output[0] = 0xEC;
  //address response
  output[1] = 0x02;
  //Active/Not active indicator
  output[2] = 0x00;
//  //Temp1 Output
//  output[3]=temp1;
//  output[4]=temp1;
//  //Temp2 Output
//  output[5]=temp2;
//  output[6]=temp1;
//  //stop bytes
  output[3] = 0x99;
  output[4] = 0x99;
  outputPacketSize = 5;
}
void writeTB() {
  writeDAC(throttle1, 1);
  writeDAC(throttle1, 2);
  writeDAC(brake1, 3);
  writeDAC(brake1, 4);
}
//reading the uart to determine packet alignment and construction
bool read485() {
  commTimer.updateTimer();
  if (commTimer.timerDone()) {
    bool stop1 = false;
    bool packetReady = false;
    bool gotAddress = false;
    bool packetRecieveStarted = false;

    if (!packetReady) {
      if (Serial.available() > 12) {
        while ((Serial.available()) && (Serial.peek() != 0xEC)) { //search for the header
          char shit;
          shit = Serial.read();     //remove bytes that are not it
        }
        if (Serial.peek() == 0xEC) { //if you found the start byte
          packetRecieveStarted = true;    //let it be recorded we found start
          count = 0;
          input[count] = Serial.read();  //record the byte
          count++;                    //increment the counter
        }
        while ((Serial.available()) && (packetRecieveStarted)) {    //if serial is there and packet is started
          if ((Serial.peek() == address) || gotAddress) { //if this is the address or we have the address already
            
            
            input[count] = Serial.read();                       //record the address byte
            
            
            if (!gotAddress) {                                        //make sure the got address indicator is set
              gotAddress = true; //set var to true to make sure we come back to this recieve location
            }
            else {                                                       //if we already set the address indicator
              //check for the stop bytes
              if ((input[count] == 0x99) && (stop1 == true)) {
                stop1 = false;
                gotAddress=false;
                packetRecieveStarted=false;
                packetReady = true;
                break;
              }
              else if (input[count] == 0x99) { // else if this is the FIRST stop byte
                stop1 = true;
                
              }
            }
            
            
            
            count++; //increment the byte forward
            delayMicroseconds(50);
          }
          else {
            stop1 = false;
             gotAddress=false;
            packetRecieveStarted=false;
            wipeBuffer();
            packetRecieveStarted = false;
            return false;
          }
        }
      }
    }
      if (packetReady) {
        switch (input[2]) {
            //request for normal operation throttle and brake update
          case 1:
            //look for the tail of the packet at appropriate location, if not get out
              parseThrottleBrake();
              writeTB();
              wipeBuffer();
              packetReady = false;
              return true;    
            //request for all stop condition
          case 2:
              //stop the car.....DEATH!!!!!!!
              toggleRelay();
              wipeBuffer();
              packetReady = false;
              return true;            
        }
        wipeBuffer();
        packetReady = false;
        return false;

      
    }
    return false;
  }
  return false;
}



//simply write the contents of the output buffer
void write485() {
  digitalWrite(RS485_direction, TALK);
  delay(1);
  for (int i = 0; i < outputPacketSize; i++) {
    Serial.write(output[i]);
  }
  delay(5);
  digitalWrite(RS485_direction, LISTEN);
}

void wipeBuffer() {
  //wipe the buffer
  for (int i = 0; i <= count; i++) {
    input[i] = 0;
  }
  count = 0;
}

union {
  int result;
  struct {
    byte low;
    byte high;
  }
  join;
}
joint;

void parseThrottleBrake() {
  joint.join.high = input[3];
  joint.join.low = input[4];
  throttle1 = joint.result;
  joint.join.high = input[5];
  joint.join.low = input[6];
  throttle2 = joint.result;
  joint.join.high = input[7];
  joint.join.low = input[8];
  brake1 = joint.result;
  joint.join.high = input[9];
  joint.join.low = input[10];
  brake2 = joint.result;
}




