//Create Prototypes for methods, makes it easier to read and handle locations

void initCommOut();
void toggleRelay();
void toggleLED();
void indicateThrottleLED();
void indicateADC_LED();
int readADC(bool ADC1, bool channel1);
void initADC();
void writeDAC(int voltage,int channel);
void initSystem();
void testWriteDAC();
void parseThrottleBrake();
void wipeBuffer();
void writeEEPROM(int voltage,int channel);
