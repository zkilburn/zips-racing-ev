
#define address 0x02

//Pin definitions
#define WDT A5
#define RS485_direction 2
#define haltPin 4
#define Relay 5
#define LED 13
#define Addr1 A0
#define Addr2 A1
#define Addr3 A2
#define Addr4 A3

#define LeftGreenLED 6
#define RightGreenLED 7 
#define LeftRedLED 8
#define RightRedLED 9

#define DashGreen 11
#define DashRed 12

#define t1 1
#define t2 2
#define b1 3
#define b2 4

#define TALK HIGH
#define LISTEN LOW
#define DAC_a 0x00
#define DAC_b 0x01
#define DAC_c 0x02
#define DAC_d 0x03

#define DAC_in 0x00
#define DAC_update 0x01
#define DAC_in_update_all 0x02
#define DAC_write_and_update 0x03
#define DAC_power_down_up 0x04
#define DAC_load_clear_code 0x05
#define DAC_load_LDAC 0x06

#define ADC 0x48

#define ADC_channel_4 0x4C
#define ADC_channel_5 0x5C

