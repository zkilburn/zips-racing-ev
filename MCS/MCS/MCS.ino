//Motor controller controller
//  systems take in throttle and brake signals from rs 485 bus
//    and repeat it out through I2C DAC to the motor controller
//
//    Features
//     - -  2 Temperature inputs for each motor (ADC1, ADC2)
//     - - I2C device communications for DAC control (1010110)  (0x56)
//                                       ADC control(1101100/1101000) (0x6C/0x68)
//                           -- AOUT1 (CH2 (1101100)) (Vout-c (1010110))
//                           -- AOUT2 (CH1 (1101100)) (Vout-a (1010110))
//                           -- AOUT3 (CH1 (1101000)) (Vout-d (1010110))
//                           -- AOUT4 (CH2 (1101000)) (Vout-b (1010110))
//     - - 485 bus communications (Serial)
//     - - 485 I/O direction control select (INT 0) D2
//     - - Interrupt pin 1 senses the halt condition (INT0)
//     - - we can trigger a halt by pulling pin D4 high
//     - - Relay control on D8
//     - - LED control on pin 12
#include "Arduino.h"
#include <Wire.h>
#include <Timers.h>
#include "defined.h"
#include "Variables.h"
#include "Methods.h"
#include "Communications.h"
#include "initializeSystem.h"
#include "DAC.h"
#include "ADC.h"
#include "Helper.h"

void setup() {
  initSystem();
  initCommOut();
  writeEEPROM(1, 0);
  writeEEPROM(2, 0);
  writeEEPROM(3, 0);
  writeEEPROM(4, 0);
}

void loop() {
  static int lastComms = millis(), commSafety = millis();
  
//  ADCTimer.updateTimer();
//  if(ADCTimer.timerDone()){
//     temp1= readADC(ADC_channel_4); 
//     temp2=readADC(ADC_channel_5);
//   output[3]=(uint8_t)((temp1&0xFF00)>>8);
// output[4]=(uint8_t)((temp1&0x00FF));
//output[5]=(uint8_t)((temp2&0xFF00)>>8);
//output[6]=(uint8_t)((temp2&0x00FF));
//}
  
  if (read485()) {
    write485();
  }
  
  
  //Check input from the motor controllers
  if (digitalRead(LeftGreenLED) && digitalRead(RightGreenLED)) {
    readyState = true;
    output[2] = 1;
    digitalWrite(DashRed, LOW);
    digitalWrite(DashGreen, HIGH);
  }
  else
  {
    output[2] = 0;
    readyState = false;
    digitalWrite(DashGreen, LOW);
    if (!(digitalRead(LeftRedLED) || digitalRead(RightRedLED))) {
      digitalWrite(DashRed, HIGH);
    }
    else {
      digitalWrite(DashRed, (digitalRead(LeftRedLED) || digitalRead(RightRedLED)));
    }
  }


  //    lastComms=millis();
  //  }
  //  else{
  //    commSafety=millis();
  //    if((commSafety-lastComms)>1000){
  //
  //    }
  //  }

  LEDTimer.updateTimer();
  if (LEDTimer.timerDone()) {
    digitalWrite(LED, !digitalRead(LED));
    //!digitalRead(Dash1));
    //digitalWrite(Dash2,!digitalRead(Dash2));
    //toggleLED();
  }
}








