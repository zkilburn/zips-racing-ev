//Motor controller controller
//  systems take in throttle and brake signals from rs 485 bus
//    and repeat it out through I2C DAC to the motor controller
//  
//    Features 
//     - -  2 Temperature inputs for each motor (ADC1, ADC2)
//     - - I2C device communications for DAC control (1010110)  (0x56)
//                                       ADC control(1101100/1101000) (0x6C/0x68)
//                           -- AOUT1 (CH2 (1101100)) (Vout-c (1010110))
//                           -- AOUT2 (CH1 (1101100)) (Vout-a (1010110))
//                           -- AOUT3 (CH1 (1101000)) (Vout-d (1010110))
//                           -- AOUT4 (CH2 (1101000)) (Vout-b (1010110))                             
//     - - 485 bus communications (Serial)
//     - - 485 I/O direction control select (INT 0) D2   
//     - - Interrupt pin 1 senses the halt condition (INT0)
//     - - we can trigger a halt by pulling pin D4 high
//     - - Relay control on D8
//     - - LED control on pin 12
#include "Arduino.h"
#include <Wire.h>
#include <Timers.h>
#include "defined.h"
#include "Variables.h"
#include "Methods.h"
#include "Communications.h"
#include "initializeSystem.h"
#include "DAC.h"
#include "ADC.h"
#include "Helper.h"

void setup() {
  initSystem();
  initCommOut();
}

void loop() { 
  static int lastComms=millis(),commSafety=millis();
  if(read485()){
    write485();
  } 
//    lastComms=millis();    
//  }  
//  else{
//    commSafety=millis();
//    if((commSafety-lastComms)>1000){
//      
//    }
//  }
  
 LEDTimer.updateTimer();
  if(LEDTimer.timerDone()){
    toggleLED();    
  }
}








