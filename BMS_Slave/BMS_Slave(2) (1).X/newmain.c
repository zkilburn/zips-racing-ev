/* 
 * File:   newmain.c
 * Author: Zac, Rick
 * BMS Slave
 * Created on January 11, 2014, 12:18 AM
 */
#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include "config.h"

#define _XTAL_FREQ 4000000

#define startByte 0xEC
#define stopByte 0x99
#define incommingLength 4
#define address 0x01
#define LED PORTCbits.RC7
#define RS485_IO PORTBbits.RB6
void putrs1USART(const char *data);
void putrsUSART(const char *data);
void putByteUSART(unsigned char data);
char readUSART(); //untested!!!!!
void selectADC(int p);
unsigned char a = 33, b = 0, udata;
int ADC = 0;
int bpon = 0; //bypas on or off
unsigned char ADCStringVal[5];
unsigned char data[5];
int counter = 0;
int heard;
int callByte;
int me;

void interrupt isr(void) {
    if (RCIF) {
        data[counter] = readUSART();
        if(data[counter]==startByte)
            callByte=1;

        if(data[counter]==address && callByte!=0)
            me=1;

        if(me!=0 || callByte!=0)
            counter++;

        if (counter > (incommingLength-1)) {
            heard = 1;
            counter = 0;
            callByte=0;
            me=0;
        }
    }
    RCIF = 0;
}

/*
 * 
 */
int main(int argc, char** argv) {
    /*
     *  I/O
     */
    TRISBbits.TRISB7 = 0; //set RB7 as output TX
    TRISBbits.TRISB6 = 0; //set RB6 as output RS485 I/O
    TRISBbits.TRISB5 = 1; //set RB5 as input  RX
    TRISBbits.TRISB4 = 0; //set RB4 as output Interupt out
    TRISCbits.TRISC7 = 0; //set RC7 as output led
    TRISCbits.TRISC6 = 1; //set RC5 as input  SJ1
    TRISCbits.TRISC3 = 1; //set RC3 as input  SJ2
    TRISAbits.TRISA5 = 1; //set RA5 as input  SJ3
    TRISCbits.TRISC5 = 1; //set RC5 as input  SJ4
    TRISCbits.TRISC4 = 1; //set RC4 as input  SJ5
    TRISAbits.TRISA4 = 1; //set RA4 as input  SJ6
    TRISAbits.TRISA2 = 1; //set RA2 as input  Interupt in
    TRISCbits.TRISC2 = 1; //set RC2 as input  Bypass
    TRISCbits.TRISC0 = 1; //set RC0 as input  Voltage
    TRISCbits.TRISC1 = 1; //set RC1 as input  NTC
    /*
     *  Analog pins that we want digital
     */
    ANSELCbits.ANSC2 = 0; //set RC2 as digital Bypass
    ANSELAbits.ANSA2 = 0; //set RA2 as digital Interupt in
    ANSELBbits.ANSB4 = 0; //set RB4 as digital Interupt out
    ANSELAbits.ANSA4 = 0; //set RA4 as digital SJ6             need to check this....
    ANSELCbits.ANSC3 = 0; //set RC3 as digital SJ2
    ANSELCbits.ANSC6 = 0; //set RC6 as digital SJ1
    ANSELCbits.ANSC7 = 0; //set RC7 as digital LED
    ANSELBbits.ANSB5 = 0; //set RB5 as digital RX
    /*
     *  UART
     */
    SPBRG = 12; // 38400 baud @ 4MHz = 12
    TXSTA = 0x24; // setup USART transmit
    CREN = 1;
    SYNC = 0;
    SPEN = 1;
    RCSTA = 0x90; // setup USART receive

    PIR1bits.RCIF = 0;
    PIE1bits.RCIE = 1;
    GIE = 1;
    PEIE = 1;
    /*
     *  ADC
     */
    FVRCONbits.ADFVR0 = 1; //vref
    FVRCONbits.ADFVR1 = 1;
    ADCON1bits.ADCS0 = 0; //Selecting the clk division factor = FOSC/4
    ADCON1bits.ADCS1 = 0;
    ADCON1bits.ADCS2 = 1;
    ADCON0bits.ADON = 1; //Turns on ADC module

    RS485_IO = 1;
    while (1) {
        if (heard != 0) {
            if(data[0]==startByte){
                if (data[1] == address)
                    if (data[2] == stopByte)
                        if (data[3] == stopByte){
                                RS485_IO = 0;
                                __delay_ms(5);
                                putByteUSART(0x01);
                                putByteUSART(0x05);
                                putByteUSART(0x05);
                                putByteUSART(0x99);
                                putByteUSART(0x99);
                                __delay_ms(5);
                                RS485_IO = 1;
                                LED = 1;
                            }

                }
            heard = 0;
        }
        __delay_ms(250); //it works, IDE bug
        //selectADC(0);
        //selectADC(1);

        LED = 0;
    }

    return (EXIT_SUCCESS);
}

void selectADC(int p) {
    if (p == 0) {
        ADCON0bits.CHS0 = 0; //Selects channel 4 ( AN4 )
        ADCON0bits.CHS1 = 0;
        ADCON0bits.CHS2 = 1;
        ADCON0bits.CHS3 = 0;
        __delay_ms(10);
        ADCON0bits.GO_nDONE = 1;
        __delay_ms(10);
        if (ADCON0bits.GO_nDONE == 0) {
            ADC = ADRES;
            //itoa(ADCStringVal,ADC,10);
            putrsUSART(" ADC Value 0 \r\n ");
            putrsUSART(itoa(ADCStringVal, ADC, 10));
            ADCON0bits.GO_nDONE = 1;
        }
    }
    if (p == 1) {
        ADCON0bits.CHS0 = 1; //Selects channel 5 ( AN5 )
        ADCON0bits.CHS1 = 0;
        ADCON0bits.CHS2 = 1;
        ADCON0bits.CHS3 = 0;
        __delay_ms(10);
        ADCON0bits.GO_nDONE = 1;
        __delay_ms(10);
        if (ADCON0bits.GO_nDONE == 0) {
            ADC = ADRES;
            //itoa(ADCStringVal,ADC,10);
            putrsUSART(" ADC Value 1 \r\n ");
            putrsUSART(itoa(ADCStringVal, ADC, 10));
            ADCON0bits.GO_nDONE = 1;
        }
    }
}

void putrsUSART(const char *data) {
    do {
        while (!(TXSTA & 0x02));
        TXREG = *data;
    } while (*data++);
}

void putrs1USART(const char *data) {
    do {
        while (!TXIF);
        TXREG = *data;
    } while (*data++);
}

void putByteUSART(unsigned char data) {
    while (!(TXSTA & 0x02));
    TXREG = data;
}

char readUSART() {
    char rx = 0;
    if (RCSTAbits.FERR)
        return 0;
    rx = RCREG;
    return rx;
}

//putrsUSART(" Test \r\n ");

//        if(PORTAbits.RA4){//if RA4 high(bypss on)...
//          //is it suposed to be on?
//            if(bpon == 0){
//              //The Bypass should not be on!!!!!
//              putrs1USART("Bypass needs to be OFF!!!! \r\n");
//            }
//        }
//        else{
//            if(bpon == 1){
//              //The Bypass should be on!!!!!
//              putrs1USART("Bypass needs to be ON!!!!! \r\n");
//            }
//        }
