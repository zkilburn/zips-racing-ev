/*
 * File:   newmain.c
 * Author: Rick
 * BMS Slave
 * Created on January 11, 2014, 12:18 AM
 */
#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "config.h"

#define _XTAL_FREQ 4000000
#define LED PORTCbits.RC7
#define BYPASS PORTCbits.RC4
#define VOLTBP 3.8
#define VOLTBPOF 3.6
#define Timeout 1000   //Comm timeout
#define startByte 0xEC
#define stopByte 0x99
#define incommingLength 4
#define address 0x05
#define LED PORTCbits.RC7
#define RS485_IO PORTBbits.RB6

void putrs1USART(const char *data);
void putrsUSART(const char *data);
void putByteUSART(unsigned char data);
char readUSART(); //untested!!!!!
int findAddress();  // this will find the hardware set Address
void selectADC(int p);
void DataOK();
void toggleLED();
void toggleBypass();
unsigned char a=33,b=0,udata;
float ADC = 0;
unsigned int ADCint=0;
int bpon = 0;  //bypas on or off
int datahere = 0;
int timecount = 0;
float volt = 0;
float temp = 0;
long counter = 0;
long laston = 0;
int LEDtime = 0;
int BPON = 0;
unsigned char ADCStringVal[5];
unsigned char data[5];
int heard;
int callByte;
int me;
int Data1 = 0;
int Data2 = 0;
//int address = 0;
/*
 *
 */

int main(int argc, char** argv) {
    /*
     *  I/O
     */
    TRISBbits.TRISB7 = 0; //set RB7 as output TX
    TRISBbits.TRISB6 = 0; //set RB6 as output RS485 I/O
    TRISBbits.TRISB5 = 1; //set RB5 as input  RX
    TRISCbits.TRISC0 = 1; //set RC0 as input  Voltage
    TRISCbits.TRISC1 = 1; //set RC1 as input  NTC
     /*
     *  Analog pins that we want digital
     */
    ANSELCbits.ANSC7 = 0; //set RC7 as digital LED
    ANSELBbits.ANSB5 = 0; //set RB5 as digital RX
     /*
     *  UART
     */
    SPBRG = 12; // 38400 baud @ 4MHz = 12
    TXSTA = 0x24; // setup USART transmit
    CREN = 1;
    SYNC = 0;
    SPEN = 1;
    RCSTA = 0x90; // setup USART receive

    PIR1bits.RCIF = 0;
    PIE1bits.RCIE = 1;
    GIE = 1;
    PEIE = 1;
     /*
     *  ADC
     */
    FVRCONbits.ADFVR0 = 1;  //vref 1.024  - 16mV accurcy
    FVRCONbits.ADFVR1 = 0;
    FVRCONbits.FVREN=1;
    
    ADCON1bits.ADCS0 = 0;   //Selecting the clk division factor = FOSC/4
    ADCON1bits.ADCS1 = 0;
    ADCON1bits.ADCS2 = 1;
    ADCON0bits.ADON = 1;    //Turns on ADC module

    //address = findAddress(); // Find the hardware address...I hope!

    PORTBbits.RB6=1;
    while(1){
        //Serial Analize here?
        //DataOK();
        toggleLED();
        selectADC(0);
        selectADC(1);
        selectADC(2);
        __delay_ms(1000);
        //toggleBypass();
    }

    return (EXIT_SUCCESS);
}
void selectADC(int p){
    if (p == 0){
      ADCON0bits.CHS0 = 0;     //Selects channel 4 ( AN4 ) Voltage
      ADCON0bits.CHS1 = 0;
      ADCON0bits.CHS2 = 1;
      ADCON0bits.CHS3 = 0;
      __delay_ms(10);
      ADCON0bits.GO_nDONE = 1;
      __delay_ms(10);
      if (ADCON0bits.GO_nDONE == 0){
         ADC = ADRES;
         //ADC filtering?
         //volt  = ((ADC/256)*4.095)*2;
         // Print out voltage

         // Convert to "gray scale"
         //Data1 = volt/256;
         //putrs1USART(" ");
         //putByteUSART(ADC);
         ADCON0bits.GO_nDONE = 1;
        }
    }
    if (p == 1){
      ADCON0bits.CHS0 = 1;     //Selects channel 5 ( AN5 ) Temp
      ADCON0bits.CHS1 = 0;
      ADCON0bits.CHS2 = 1;
      ADCON0bits.CHS3 = 0;
      __delay_ms(10);
      ADCON0bits.GO_nDONE = 1;
      __delay_ms(10);
      if (ADCON0bits.GO_nDONE == 0){
         ADC = ADRES;
         /*ADC filtering?
         temp = (256 / ADC) - 1.0;
         temp = 1000.0 / temp;
         temp = temp / 1000.0;                   // (R/Ro)
         temp = log(temp);                       // ln(R/Ro)
         temp = temp / 3348.0;                    // 1/B * ln(R/Ro)
         temp = temp + 0.0033540164;              // + (1/To)
         temp = 1.0 / temp;                       // Invert
         temp = temp - 273.15;                    // convert to C
          // itoa((int)temp,ADCStringVal);
          */
         //putrs1USART(" Volt ");
         //putByteUSART(ADC);
         ADCON0bits.GO_nDONE = 1;
        }
    }
    if(p==2){
      ADCON0bits.CHS0 = 1;     //Selects channel 5 ( AN5 ) Temp
      ADCON0bits.CHS1 = 1;
      ADCON0bits.CHS2 = 1;
      ADCON0bits.CHS3 = 1;
      __delay_ms(10);
      ADCON0bits.GO_nDONE = 1;
      __delay_ms(10);
      if (ADCON0bits.GO_nDONE == 0){
         ADC = ADRES;
         ADC=  78.046*exp(-0.011*ADC);//log(ADC);//(-0.318*ADC)+59.166;
         ADC+=2;
         /*ADC filtering?
         temp = (256 / ADC) - 1.0;
         temp = 1000.0 / temp;
         temp = temp / 1000.0;                   // (R/Ro)
         temp = log(temp);                       // ln(R/Ro)
         temp = temp / 3348.0;                    // 1/B * ln(R/Ro)
         temp = temp + 0.0033540164;              // + (1/To)
         temp = 1.0 / temp;                       // Invert
         temp = temp - 273.15;                    // convert to C
          // itoa((int)temp,ADCStringVal);
          */
         //putrs1USART(" Volt ");
         ADCint=(unsigned int)ADC;
         putByteUSART((ADCint&0xFF00)>>8);
         putByteUSART((ADCint&0x00FF)>>0);;
         ADCON0bits.GO_nDONE = 1;
    }
    }
}

void putrsUSART(const char *data)
{
do
{
while(!(TXSTA & 0x02));
TXREG = *data;
} while( *data++ );
}

void putrs1USART(const char *data)
{
do
{
while(!TXIF);
TXREG = *data;
} while( *data++ );
}

void putByteUSART(unsigned char data)
{


while(!(TXSTA & 0x02));
TXREG = data;

}

char readUSART()
{
   char rx = 0;
   rx = RCREG;
   return rx;
}

void toggleLED() {
    //if (laston > 100) {
        LED = !LED;
   //     laston=0;
   // } else laston++;
}





