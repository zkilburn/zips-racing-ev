

void checkBootTime(){
   
}

void driveStart() {
  while (!readyToDrive) {

    static Timers SSTimeout(10000);
    throttleUpdateCheck();
    throttleDataCheck();
    toggleLED();
    buttonTEST();
    SSTimeout.resetTimer();
    
    while (startBoot) {
      throttleUpdateCheck();
      toggleLED();
      buttonTEST();
      SSTimeout.updateTimer();
      if (SSTimeout.timerDone()) {
        digitalWrite(SS, LOW);
        digitalWrite(BLED2, LOW);
        startBoot = false;
        toggleRelay = false;
        break;
      }

      while (!BMSready&& startBoot) {
        SSTimeout.updateTimer();
          if (SSTimeout.timerDone()) {
            digitalWrite(SS, LOW);
            digitalWrite(BLED2, LOW);
            startBoot = false;
            toggleRelay = false;
            break;
          }
          toggleLED();
          buttonTEST();          
        requestFaultUpdate();
      }

      if ((readSS() > 300)) {
        //    if ((digitalRead(17) == 1) && (digitalRead(6) == 1) && (brake1 <= 1000)) {
        digitalWrite(SS, HIGH);
        toggleRelay = false;

        while (digitalRead(EXT)&& startBoot) {
          SSTimeout.updateTimer();
          if (SSTimeout.timerDone()) {
            digitalWrite(SS, LOW);
            digitalWrite(BLED2, LOW);
            startBoot = false;
            toggleRelay = false;
            break;
          }
          toggleLED();
          buttonTEST();
          if ((readSS() < 300) || toggleRelay) {
            digitalWrite(SS, LOW);
            digitalWrite(BLED2, LOW);
            startBoot = false;
            toggleRelay = false;
            break;
          }
          //Serial.println("SS attempting to lock ");
          delay(5);
        }


        bool MCSready = false;
        while (!MCSready && startBoot) {

          //Serial.println("MCS feedback");
          toggleLED();
          buttonTEST();
          SSTimeout.updateTimer();
          if (SSTimeout.timerDone()) {
            digitalWrite(SS, LOW);
            digitalWrite(BLED2, LOW);
            startBoot = false;
            toggleRelay = false;
            break;
          }
           if ((readSS() < 300) || toggleRelay) {
            digitalWrite(SS, LOW);
            digitalWrite(BLED2, LOW);
            startBoot = false;
            toggleRelay = false;
            break;
          }
          if (readMCS()) {
            MCSready = MCSstable;
          }
        }

        if ((readSS() > 300) && !digitalRead(EXT) && (MCSready != 0)) {
          blowHorn(2500);
          readyToDrive = true;
          startBoot = false;
          faultLock = false;
          toggleRelay = false;
          digitalWrite(BLED2, HIGH);
        }
      }
    }
  }
}

