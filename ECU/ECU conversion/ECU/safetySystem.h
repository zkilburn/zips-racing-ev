void faultSystem() {
  digitalWrite(SS, LOW);
  digitalWrite(BLED2, LOW);
  readyToDrive = false;
  MCSstable = false;
  BMSready = false;
  throttle1 = 0;
  throttle2 = 0;
  brake1 = 0;
  brake2 = 0;
  sendDataMCS();
  //sendStopMCS();
  driveStart();
}
bool flagTBCompare = false;
bool throttleBrakeCompare() {

  if (flagTBCompare) {
    throttle1=0;
    throttle2=0;
    if ((throttle1 < 15) && (brake2 < 25)) {
      flagTBCompare = false;
      Serial.println("TB fault clear");
    }
  }
  else if (throttle1 > 25) {
    if (brake2 > 25) {
      flagTBCompare = true;
      Serial.println("TB fault set");
    }
  }

  if (flagTBCompare)
    return true;
  else
    return false;


}


bool throttleDataCheck() {


  static Timers speedTimer(1000);
  speedTimer.updateTimer();
  if (speedTimer.timerDone()) {
    //requestBatteryInformations();
    //   requestFaultUpdate();
    //
    //    requestBatteryInformations();
    //  readMCS();
    //  Serial.print("Current Usage:  ");
    //  Serial.println(current);
    //  Serial.println(" ");
    //  Serial.print("Throttle 1: ");
    //  Serial.println(throttle1);
    //  Serial.print("Throttle 2: ");
    //  Serial.println(throttle2);
    //  Serial.print("Brake 1   : ");
    //  Serial.println(brake1);
    //  Serial.print("Brake 2   : ");
    //  Serial.println(brake2);
    if ((speedFrontLeft > 0) || (speedFrontRight > 0) || (speedRearLeft > 0) || (speedRearRight > 0)) {
      Serial.println(" ");
      Serial.print("Wheel speed front left:  ");
      Serial.println((speedFrontLeft * 0.4351));
      Serial.print("Wheel Speed front right:  ");
      Serial.println((speedFrontRight * 0.4351));
      Serial.print("Wheel Speed Rear left:  ");
      Serial.println((speedRearLeft * 0.85227));
      Serial.print("Wheel Speed Rear Right:  ");
      Serial.println((speedRearRight * 0.85227)); //     *3600/5280
      speedRearRight = 0;
      speedRearLeft = 0;
      speedFrontRight = 0;
      speedFrontLeft = 0;
    }
  }


  if (throttleBrakeCompare()) {
    return false;
  }
  if (throttle2 < 11) {
    if ((throttle2 < throttle1 + 10) && (throttle1 < throttle2 + 10))  {
      throttle1 = throttle1 * 40.95;
      throttle2 = throttle2 * 40.95;

      return true;
    }
    else
      return false;
  } else
  {
    if ((throttle1 > throttle2 - 10) && (throttle1 < throttle2 + 10))  {
      throttle1 = throttle1 * 40.95;
      throttle2 = throttle2 * 40.95;
      return true;
    }
    else
      return false;
  }


  return false;

}
