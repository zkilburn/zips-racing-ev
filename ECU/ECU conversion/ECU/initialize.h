

void setupPins() {

  //EXT
  pinMode(EXT, INPUT);
  //RTG
  pinMode(RTG, INPUT);
  //SS
  pinMode(SS, OUTPUT);
  //RTD Horn
  pinMode(HORN, OUTPUT);
  //Button A
  pinMode(BUTA, INPUT);
  //Button B
  pinMode(BUTB, INPUT);
  //Button LED
  pinMode(BLED1, OUTPUT);
  pinMode(BLED2, OUTPUT);
  digitalWrite(BLED2, LOW);
  digitalWrite(SS, LOW);
  digitalWrite(RTG, HIGH);
  digitalWrite(EXT, HIGH);
}

void initializeCommSystems() {
  //turn on serial comm

  //repurposed 328 transciever - SAS/BMS
  Serial1.begin(38400);
  pinMode(TLBMSSAS, OUTPUT);
  digitalWrite( TLBMSSAS, HIGH);


  //Main transciever - MCS
  Serial2.begin(38400);
  pinMode(TLMCS, OUTPUT);
  digitalWrite(TLMCS, HIGH);


  //DEBUG USB
  Serial.begin(38400);
}
