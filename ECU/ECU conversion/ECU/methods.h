

void initializeCommSystems();
void sendDataMCS();
bool readSAS();
void requestBMSupdate();
void setupPins();
bool variableIsAbout(int var1, int var2, int range);
void driveStart();
void blowHorn(int duration);
void toggleLED();
void buttonTEST();
int readSS();
void request_SAS_All();
void throttleUpdateCheck();
void requestBatteryInformations();
void requestFaultUpdate();
bool readMCS();
bool responseMCS();
void sendStopMCS();


bool variableIsAbout(int var1, int var2, int range) {
  return ((var1 < var2 + range) && (var1 < var2 - range));
}








