bool MCSresponse() {
  if (Serial2.available() >= 5) {
    if (Serial2.read() == 0xEC) {
      if (Serial2.read() == MCSADDRESS) {
        if (Serial2.read() == 0x00) {
          //faultLock = true;
          MCSstable = false;
          //Serial.println("MCS detects no MC");
        }
        else {
          MCSstable = true;
        }

        Serial2.read();
        Serial2.read();
        return true;
      }
    }
  }
  return false;
}
bool readSAS() {
  static char packet[20];
  if (Serial1.available() > 17)
    while (Serial1.available()) {
      if (Serial1.read() == 0xEC) {
        if (Serial1.read() == SASADDRESS) {
          switch (Serial1.read()) { //switch the command to be read in
            case SAS_REQUEST_TB:
              for (int i = 0; i < 10; i++) {
                packet[i] = Serial1.read();
              }
              joint.join.high = packet[0];
              joint.join.low = packet[1];
              throttle1 = joint.result;


              joint.join.high = packet[2];
              joint.join.low = packet[3];
              throttle2 = joint.result;


              joint.join.high = packet[4];
              joint.join.low = packet[5];
              brake1 = joint.result;

              if (brake1 < 0) {
                brake1 = 0;
              }
              if (brake1 > 100) {
                brake1 = 100;
              }

              joint.join.high = packet[6];
              joint.join.low = packet[7];
              brake2 = joint.result;
              if (brake2 < 0) {
                brake2 = 0;
              }
              if (brake2 > 100) {
                brake2 = 100;
              }

              if (packet[8] == packet[9]) {
                return true;
              }
              break;
            case SAS_REQUEST_WS:
              speedFrontLeft = (Serial1.read() << 8) & (Serial1.read());
              speedFrontRight = (Serial1.read() << 8) & (Serial1.read());
              speedRearLeft = (Serial1.read() << 8) & (Serial1.read());
              speedRearRight = (Serial1.read() << 8) & (Serial1.read());
              if (Serial1.read() == 0x99 && Serial1.read() == 0x99)
                return true;
              break;
            case SAS_REQUEST_ALL:
              for (int i = 0; i < 18; i++) {
                packet[i] = Serial1.read();
              }


              joint.join.high = packet[0];
              joint.join.low = packet[1];
              throttle1 = joint.result;
              throttle1 -= 5;
              if (((int)throttle1) < 0) {
                throttle1 = 0;
              }

              if (throttle1 > 50)
              {
                throttle1 * 1.1;
              }

              if (throttle1 > 100) {
                throttle1 = 100;
              }

              joint.join.high = packet[2];
              joint.join.low = packet[3];
              throttle2 = joint.result;
              throttle2 -= 5;

              if (((int)throttle2) < 0) {
                throttle2 = 0;
              }

              if (throttle2 > 50) {
                throttle2 * 1.1;
              }

              if (throttle2 > 100) {
                throttle2 = 100;
              }



              joint.join.high = packet[4];
              joint.join.low = packet[5];
              brake1 = joint.result;

//              if ((i  nt)(brake1) < 0) {
//                brake1 = 0;
//              }
//              if (brake1 > 100) {
//                brake1 = 100;
//              }



              joint.join.high = packet[6];
              joint.join.low = packet[7];
              brake2 = joint.result;
//
//              if ((int)(brake2) < 0) {
//                brake2 = 0;
//              }
//              if (brake2 > 100) {
//                brake2 = 100;
//              }

              joint.join.high = packet[8];
              joint.join.low = packet[9];
              speedRearLeft += joint.result;
              joint.join.high = packet[10];
              joint.join.low = packet[11];
              speedRearRight += joint.result;
              joint.join.high = packet[12];
              joint.join.low = packet[13];
              speedFrontRight += joint.result;
              joint.join.high = packet[14];
              joint.join.low = packet[15];
              speedFrontLeft += joint.result;
              if ((packet[16] == packet[17])) {
                return true;
              }
              break;
          }
        }
      }
    }
  return false;

}

bool readBMSResponse() {
  if (Serial1.available() >= 8) {
    while (Serial1.available()) {  //BMS
      if (faultFlag) {
        if (Serial1.read() == startByte)
          if (Serial1.read() == BMSADDRESS)
            if (Serial1.read() == FAULTUPDATE)
              switch (Serial1.read()) {
                case 1:
                  joint.join.high = Serial1.read();
                  joint.join.low = Serial1.read();
                  current = joint.result;
                  if (Serial1.read() == Serial1.read()) {
                    BMSready = true;
                    faultFlag = 0;
                    return true;
                  }

                  break;
                case 0:
                  joint.join.high = Serial1.read();
                  joint.join.low = Serial1.read();
                  current = joint.result;
                  Serial.println("BMS Slave Comms Fault");
                  if (Serial1.read() == Serial1.read()) {
                    //faultLock = true;
                    //BMSready = false;
                    BMSready=true;
                    faultFlag = 0;
                    return true;
                  }
                  break;
                  case 3:
                  joint.join.high = Serial1.read();
                  joint.join.low = Serial1.read();
                  current = joint.result;
                  Serial.println("BMS Slave voltage low");
                  if (Serial1.read() == Serial1.read()) {
                    faultLock = true;
                    BMSready = false;
                    faultFlag = 0;
                    return true;
                  }
                  break;
                  
                  case 4:
                  joint.join.high = Serial1.read();
                  joint.join.low = Serial1.read();
                  current = joint.result;
                  Serial.println("BMS Slave Voltage High");
                  if (Serial1.read() == Serial1.read()) {
                    faultLock = true;
                    BMSready = false;
                    faultFlag = 0;
                    return true;
                  }
                  break;
              }
      }
      else if (Serial1.read() == startByte)
        if (Serial1.read() == BMSADDRESS)
          if (Serial1.read() == BATTERYUPDATE) {
            for (int i = 0; i < numBatteries; i++) {
              if (i > 0) {
                batteryVolt[i] = (Serial1.read());
                batteryTemp[i] = Serial1.read();
                Serial.print("Batt #");
                Serial.print(i);
                Serial.print(":  ");
                Serial.print(batteryVolt[i]);
                Serial.print("  T: ");
                Serial.println(batteryTemp[i]);
                delayMicroseconds(250);
              }
              else {
                batteryVolt[i] = (Serial1.read());
                batteryTemp[i] = Serial1.read();
              }

              if (!Serial1.available()) {
                break;
              }
            }
            Serial.print("Current measure: ");
            Serial.println(current);
            if (Serial1.read() == Serial1.read()) {

              return true;
            }

          }
    }
    return false;
  }
  else
    return false;
}
