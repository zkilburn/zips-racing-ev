

void toggleLED() {
  static int SSin = 0;
  LEDTimer.updateTimer();
  LED2Timer.updateTimer();
  if (LED2Timer.timerDone()) {
    if (!readyToDrive && startBoot) {
      if ((readSS() > 300)) {
        digitalWrite(BLED2, !digitalRead(BLED2));
        //Serial.println("SS firing");
      }
      else {
        if (SSin > 4) {
          digitalWrite(BLED2, !digitalRead(BLED2));
          // Serial.println("SS input missing");
          SSin = 0;
        } else
          SSin++;
      }
    }
    else {

    }
  }
  
  if (LEDTimer.timerDone()) {
    digitalWrite(13, !digitalRead(13));
    //digitalWrite(BLED1, !digitalRead(BLED1));
  }
  
}

void buttonTEST() {

  if (!digitalRead(BUTA)) {
    requestBatteryInformations();
  }
  else {
    
  }

  if (digitalRead(BUTB)) {
    readyToggle = true;
  }
  else {
    if (readyToggle) {
      digitalWrite(BLED2, !digitalRead(BLED2));
      toggleRelay = true;
      if (!startBoot && (!readyToDrive && ((brake2 > 0)))) {
        startBoot = true;
      }
      else {
        if (!readyToDrive)
          digitalWrite(BLED2, LOW);
        startBoot = false;
      }
      readyToggle = false;
    }
  }

}
