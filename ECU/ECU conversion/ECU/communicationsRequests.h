




void request_SAS_All() {
  digitalWrite( TLBMSSAS, HIGH);
  delayMicroseconds(250);
  Serial1.write(startByte);
  Serial1.write(SASADDRESS);
  Serial1.write(SAS_REQUEST_ALL);
  Serial1.write(stopByte);
  Serial1.write(stopByte);
  delay(4);
  digitalWrite( TLBMSSAS, LOW);
}

void request_SAS_TB() {
  digitalWrite( TLBMSSAS, HIGH);
  delayMicroseconds(250);
  Serial1.write(startByte);
  Serial1.write(SASADDRESS);
  Serial1.write(SAS_REQUEST_TB);
  Serial1.write(stopByte);
  Serial1.write(stopByte);
  delay(4);
  digitalWrite( TLBMSSAS, LOW);
}

void requestBMSFaults() {
  faultFlag = true;
  digitalWrite( TLBMSSAS, HIGH);
  delayMicroseconds(250);
  Serial1.write(startByte);
  Serial1.write(BMSADDRESS);
  Serial1.write(FAULTUPDATE);
  Serial1.write(stopByte);
  Serial1.write(stopByte);
  delay(2);
  digitalWrite( TLBMSSAS, LOW);
}

void requestBatteryInformation() {
  batteryFlag = true;
  digitalWrite( TLBMSSAS, HIGH);
  delayMicroseconds(250);
  Serial1.write(startByte);
  Serial1.write(BMSADDRESS);
  Serial1.write(BATTERYUPDATE);
  Serial1.write(stopByte);
  Serial1.write(stopByte);
  delay(2);
  digitalWrite( TLBMSSAS, LOW);

}

void sendStopMCS() {
  digitalWrite(TLMCS, HIGH);
  delay(1);
  Serial2.write(startByte);
  Serial2.write(MCSADDRESS);
  Serial2.write(MCSSTOP);
  Serial2.write(stopByte);
  Serial2.write(stopByte);
  delay(4);
  digitalWrite(TLMCS, LOW);
}

void sendDataMCS() {
  digitalWrite(TLMCS, HIGH);
  delay(1);
  Serial2.write(startByte);
  Serial2.write(MCSADDRESS);
  Serial2.write(MCSUPDATE);
  Serial2.write((uint8_t)((throttle1 & 0xFF00) >> 8));
  Serial2.write((uint8_t)(throttle1 & 0x00FF));
  Serial2.write((uint8_t)((throttle2 & 0xFF00) >> 8));
  Serial2.write((uint8_t)(throttle2 & 0x00FF));
  Serial2.write((uint8_t)((brake1 & 0xFF00) >> 8));
  Serial2.write((uint8_t)(brake1 & 0x00FF));
  Serial2.write((uint8_t)((brake2 & 0xFF00) >> 8));
  Serial2.write((uint8_t)(brake2 & 0x00FF));
  Serial2.write(stopByte);
  Serial2.write(stopByte);
  delay(4);
  digitalWrite(TLMCS, LOW);
}
