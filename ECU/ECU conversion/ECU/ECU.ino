#include "Arduino.h"
#include <Timers.h>
#include "definitions.h"
#include "variables.h"
#include "methods.h"
#include "initialize.h"
#include "inputs.h"
#include "outputs.h"
#include "safetySystem.h"
#include "bootCar.h"
#include "communicationsReceive.h"
#include "communicationsRequests.h"
#include "communicationsHandlers.h"


int readSS() {
  return analogRead(SStest);
}
void checkSS() {
  if ( (readSS() < 300)) {
    faultLock = true;
  }
  if (toggleRelay) {
    faultLock = true;
    toggleRelay = false;
  }
}

void setup() {
  setupPins();
  pinMode(13, OUTPUT);
  initializeCommSystems();
  driveStart();
}

void loop() {
  toggleLED();
  buttonTEST();
  SSread = readSS();
  throttleUpdateCheck();
  if (throttleDataCheck()) {
    Serial.print("T1: ");
    Serial.print(throttle1);
    Serial.print("   T2: ");
    Serial.print(throttle2);
    Serial.print("  B1: ");
    Serial.print(brake1);
    Serial.print("  B2: ");
    Serial.println(brake2);
    throttleOut = (uint16_t)((throttle1 + throttle2) / 2) * 40.95;
    brakeOut = (uint16_t)((brake2)) * 40.95;
    readMCS();
  }
  else {
    Serial.print("Failed.. T1: ");
    Serial.print(throttle1);
    Serial.print("   T2: ");
    Serial.print(throttle2);
    Serial.print("  B1: ");
    Serial.print(brake1);
    Serial.print("  B2: ");
    Serial.println(brake2);
    throttle1=0;
    throttle2=0;
    brake1=0;
    brake2=0;
    readMCS();
    //faultLock = true;
  }

  requestFaultUpdate();
  //requestBatteryInformations();
  checkSS();
  //digitalWrite(BLED1,digitalRead(EXT));
  if (digitalRead(EXT) && (heardItOnce > 5)) {
    faultLock = true;
  } else if (digitalRead(EXT)) {
    heardItOnce++;
  }
  else if (!digitalRead(EXT)) {
    heardItOnce = 0;
  }

  if (faultLock) {
    heardItOnce = 0;
    faultLock = false;
    faultSystem();
  }
}









void printSASdataXBEE() {

}
