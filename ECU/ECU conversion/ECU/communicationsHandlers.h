void requestBatteryInformations() {

  notListening = 0;
  unheard = 0;
  requestBatteryInformation();
  while (!readBMSResponse()) {
    delay(5);
    unheard++;
    if (unheard > 50) {
      unheard = 0;
      notListening++;
      if (notListening > 5) {
        Serial.println("BMS comms faulted");
        notListening = 0;
        faultLock = true;
        break;
      }
      requestBatteryInformation();
    }
  }
  notListening = 0;
  unheard = 0;
}
void requestFaultUpdate() {

  notListening = 0;
  unheard = 0;
  requestBMSFaults();
  while (!readBMSResponse()) {
    delay(5);
    unheard++;
    if (unheard > 30) {
      unheard = 0;
      notListening++;
      if (notListening > 5) {
        notListening = 0;
        faultLock = true;
        Serial.println("BMS comms faulted");
        break;
      }
      requestBMSFaults();
    }
  }
  notListening = 0;
  unheard = 0;
}

void throttleUpdateCheck() {

  notListening = 0;
  unheard = 0;
  //request_SAS_TB();
  request_SAS_All();
  while (!readSAS()) {
    delay(5);
    unheard++;
    if (unheard > 30) {
      unheard = 0;
      notListening++;
      if (notListening > 5) {
        notListening = 0;
        faultLock = true;

        Serial.println("SAS comms faulted");
        break;
      }
      //request_SAS_TB();
      request_SAS_All();
    }
  }
  notListening = 0;
  unheard = 0;
}

bool readMCS() {
  // Serial2.flush();
  sendDataMCS();
  notListening = 0;
  unheard = 0;
  while (!MCSresponse()) {
    delay(5);
    unheard++;
    if (unheard > 30) {
      unheard = 0;
      notListening++;
      if (notListening > 5) {
        notListening = 0;
        faultLock = true;
        Serial.println("MCS comms faulted");
        return false;
      }
      sendDataMCS();
    }
  }
  notListening = 0;
  unheard = 0;
  return true;
}
