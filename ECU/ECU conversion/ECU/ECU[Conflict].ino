//Talk listen pin (direction 485) settings
#define TLBMSSAS 8   //3
#define TLMCS A15 //27

#define ECUADDRESS 0x00

#define MCSADDRESS 0x02
#define MCSUPDATE 0x01
#define MCSSTOP 0x02

#define SASADDRESS 0x01
#define SAS_REQUEST_WS  0x02
#define SAS_REQUEST_TB 0x01
#define SAS_REQUEST_ALL 0x03

#define BMSADDRESS 0x03
#define FAULTUPDATE 0x01
#define BATTERYUPDATE 0x02
#define numBatteries 28

#define startByte 0xEC
#define stopByte 0x99

bool faultFlag = false;
bool batteryFlag = false;
bool faultLock = false;
int inByte = 0;         // incoming serial byte
uint16_t throttle1 = 0, throttle2 = 0;
uint16_t brake1 = 0, brake2 = 0;
uint16_t speedFrontLeft = 0, speedFrontRight = 0;
uint16_t speedRearLeft = 0, speedRearRight = 0;
uint8_t batteryInfo[28];
bool SASRequestFinish = false;
bool readyToDrive = false;

void initializeCommSystems();
void sendDataMCS();
bool readSAS();
void requestBMSupdate();
void setupPins();
bool variableIsAbout(int var1, int var2, int range);
void driveStart();
void blowHorn(int duration);

void blowHorn(int duration) {
  digitalWrite(11, HIGH);
  delay(duration);
  digitalWrite(11, LOW);
}

void setup() {
  setupPins();
  pinMode(13, OUTPUT);
  initializeCommSystems();
  //driveStart();
}

void setupPins() {
  //RTG
  pinMode(7, OUTPUT);
  //SS
  pinMode(5, OUTPUT);
  //RTD Horn
  pinMode(11, OUTPUT);
  //Button A
  pinMode(17, INPUT);
  //Button B
  pinMode(18, INPUT);
  //Button LED
  pinMode(4, OUTPUT);
}

void initializeCommSystems() {
  //turn on serial comm


  //repurposed 328 transciever - SAS/BMS
  Serial3.begin(38400);
  pinMode(TLBMSSAS, OUTPUT);
  digitalWrite( TLBMSSAS, HIGH);


  //Main transciever - MCS
  Serial2.begin(38400);
  pinMode(TLMCS, OUTPUT);
  digitalWrite(TLMCS, HIGH);


  //DEBUG USB
  Serial.begin(38400);
}

void driveStart() {
  while (!readyToDrive) {
    if ((digitalRead(17) == 1) && (digitalRead(6) == 1) && (brake1 <= 1000)) {
      digitalWrite(5, HIGH);
      for (int i = 0; i < 6; i++)
        delay(1000);
      blowHorn(3000);
      readyToDrive = true;
      break;
    }
  }
}

void faultSystem() {
  digitalWrite(5, LOW);
  while (1) {
    delay(500);
    digitalWrite(13, !digitalRead(13));
  }
}

void loop() {

  throttleUpdateCheck();
  if (throttleDataCheck()) {
    sendDataMCS();
  }
  else {
    faultLock = true;
  }
  //readMCS();
  //requestFaultUpdate();
  digitalWrite(13, !digitalRead(13));
  if (faultLock) {
    //faultSystem();
  }
}

bool throttleDataCheck() {

  Serial.println();
  Serial.print("Throttle 1: ");
  Serial.println(throttle1);
  Serial.print("Throttle 2: ");
  Serial.println(throttle2);
  Serial.print("Brake 1   : ");
  Serial.println(brake1);
  Serial.print("Brake 2   : ");
  Serial.println(brake2);
  Serial.println();
  if (throttle2 < 11) {
    if ((throttle2 < throttle1 + 10) && (throttle1 < throttle2 + 10))  {
      throttle1 = throttle1 * 40.95;
      throttle2 = throttle2 * 40.95;
      Serial.print("Throttle 1: ");
      Serial.println(throttle1);
      Serial.print("Throttle 2: ");
      Serial.println(throttle2);
      return true;
    }
    else
    return false;
  } else
  {
    if ((throttle1 > throttle2 - 10) && (throttle1 < throttle2 + 10))  {
      throttle1 = throttle1 * 40.95;
      throttle2 = throttle2 * 40.95;
      Serial.print("Throttle 1: ");
      Serial.println(throttle1);
      Serial.print("Throttle 2: ");
      Serial.println(throttle2);
      return true;
    }
    else 
     return false;
  }
}

int unheard = 0;
int notListening = 0;

void throttleUpdateCheck() {
  request_SAS_TB();
  delay(10);
  while (!readSAS()) {
    delay(5);
    unheard++;
    if (unheard > 50) {
      unheard = 0;
      notListening++;
      if (notListening > 5) {
        notListening = 0;
        faultLock = true;
        break;
      }
      request_SAS_All();
    }
  }
  notListening = 0;
  unheard = 0;
}

void request_SAS_All() {
  digitalWrite( TLBMSSAS, HIGH);
  delay(1);
  Serial3.write(startByte);
  Serial3.write(SASADDRESS);
  Serial3.write(SAS_REQUEST_ALL);
  Serial3.write(stopByte);
  Serial3.write(stopByte);
  delay(4);
  digitalWrite( TLBMSSAS, LOW);
}

void request_SAS_TB() {
  digitalWrite( TLBMSSAS, HIGH);
  delay(1);
  Serial3.write(startByte);
  Serial3.write(SASADDRESS);
  Serial3.write(SAS_REQUEST_TB);
  Serial3.write(stopByte);
  Serial3.write(stopByte);
  delay(4);
  digitalWrite( TLBMSSAS, LOW);
}

union {
  int result;
  struct {
    byte low;
    byte high;
  }
  join;
}
joint;

bool readSAS() {
  static char packet[20];
  if (Serial3.available() > 9)
    if (Serial3.read() == 0xEC) {
      if (Serial3.read() == SASADDRESS) {
        switch (Serial3.read()) { //switch the command to be read in
          case SAS_REQUEST_TB:
            for (int i = 0; i < 10; i++) {
              packet[i] = Serial3.read();
            }
            joint.join.high = packet[0];
            joint.join.low = packet[1];
            throttle1 = joint.result;
            joint.join.high = packet[2];
            joint.join.low = packet[3];
            throttle2 = joint.result;
            joint.join.high = packet[4];
            joint.join.low = packet[5];
            brake1 = joint.result;
            joint.join.high = packet[6];
            joint.join.low = packet[7];
            brake2 = joint.result;
            if (packet[8] == 0x99 && packet[9] == 0x99)
              return true;
            break;
          case SAS_REQUEST_WS:
            speedFrontLeft = (Serial3.read() << 8) & (Serial3.read());
            speedFrontRight = (Serial3.read() << 8) & (Serial3.read());
            speedRearLeft = (Serial3.read() << 8) & (Serial3.read());
            speedRearRight = (Serial3.read() << 8) & (Serial3.read());
            if (Serial3.read() == 0x99 && Serial3.read() == 0x99)
              return true;
            break;
          case SAS_REQUEST_ALL:
            for (int i = 0; i < 18; i++) {
              packet[i] = Serial3.read();
            }
            joint.join.high = packet[0];
            joint.join.low = packet[1];
            throttle1 = joint.result;
            joint.join.high = packet[2];
            joint.join.low = packet[3];
            throttle2 = joint.result;
            joint.join.high = packet[4];
            joint.join.low = packet[5];
            brake1 = joint.result;
            joint.join.high = packet[6];
            joint.join.low = packet[7];
            brake2 = joint.result;
            speedFrontLeft = (packet[8] << 8) & (packet[9]);
            speedFrontRight = (packet[10] << 8) & (packet[11]);
            speedRearLeft = (packet[12] << 8) & (packet[13]);
            speedRearRight = (packet[14] << 8) & (packet[15]);
            if (packet[16] == 0x99 && packet[17] == 0x99)
              return true;
            break;
        }
      }
      else return false;
    }
    else {
      delay(5);
      return false;
    }
}

void sendDataMCS() {
  digitalWrite(TLMCS, HIGH);
  delay(1);
  Serial2.write(startByte);
  Serial2.write(MCSADDRESS);
  Serial2.write(MCSUPDATE);
  Serial2.write(((throttle1 & 0xFF00) >> 8));
  Serial2.write((uint8_t)(throttle1 & 0x00FF));
  Serial2.write((uint8_t)((throttle2 & 0xFF00) >> 8));
  Serial2.write((uint8_t)(throttle2 & 0x00FF));
  Serial2.write((uint8_t)((brake1 & 0xFF00) >> 8));
  Serial2.write((uint8_t)(brake1 & 0x00FF));
  Serial2.write((uint8_t)((brake2 & 0xFF00) >> 8));
  Serial2.write((uint8_t)(brake2 & 0x00FF));
  Serial2.write(stopByte);
  Serial2.write(stopByte);
  delay(4);
  digitalWrite(TLMCS, LOW);
}

void readMCS() {
  while (!(Serial2.available())) {
    delay(5);
    unheard++;
    if (unheard > 50) {
      unheard = 0;
      notListening++;
      if (notListening > 5) {
        notListening = 0;
        faultLock = true;
        break;
      }
      sendDataMCS();
    }
  }
  notListening = 0;
  unheard = 0;
  while (Serial2.available())    {
    Serial2.read();
  }
}

void sendStopMCS() {
  digitalWrite(TLMCS, HIGH);
  delay(1);
  Serial2.write(startByte);
  Serial2.write(MCSADDRESS);
  Serial2.write(MCSSTOP);
  Serial2.write(stopByte);
  Serial2.write(stopByte);
  delay(4);
  digitalWrite(TLMCS, LOW);
}

bool readBMSResponse() {
  if (Serial3.available() > 6) {
    while (Serial3.available()) {  //BMS
      if (faultFlag) {
        if (Serial3.read() == startByte)
          if (Serial3.read() == BMSADDRESS)
            if (Serial3.read() == FAULTUPDATE)
              switch (Serial3.read()) {
                case 1:
                  if (Serial3.read() == 0x99)
                    if (Serial3.read() == 0x99) {
                      Serial.println();
                      Serial.println("BMS Fault Pass");
                      Serial.println();
                    }
                  break;
                case 0:
                  if (Serial3.read() == 0x99)
                    if (Serial3.read() == 0x99) {
                      faultLock = true;
                    }
                  break;
              }
        faultFlag = 0;
        return true;
      }
      else if (Serial3.read() == startByte)
        if (Serial3.read() == BMSADDRESS)
          if (Serial3.read() == BATTERYUPDATE)
            for (int i = 0; i < numBatteries; i++) {
              batteryInfo[i] = (Serial3.read());
            }
      if (Serial3.read() == 0x99)
        if (Serial3.read() == 0x99) {

        }
    }
    return true;
  }
  else {
    delay(5);
    return false;
  }
}

void requestBMSFaults() {
  faultFlag = true;
  digitalWrite( TLBMSSAS, HIGH);
  delay(1);
  Serial3.write(startByte);
  Serial3.write(BMSADDRESS);
  Serial3.write(FAULTUPDATE);
  Serial3.write(stopByte);
  Serial3.write(stopByte);
  delay(4);
  digitalWrite( TLBMSSAS, LOW);
}

void requestFaultUpdate() {
  requestBMSFaults();
  delay(10);

  while (!readBMSResponse()) {
    delay(5);
    unheard++;
    if (unheard > 50) {
      unheard = 0;
      notListening++;
      if (notListening > 5) {
        notListening = 0;
        faultLock = true;
        break;
      }
      requestBMSFaults();
    }
  }
  notListening = 0;
  unheard = 0;
}

void requestBatteryInformation() {
  batteryFlag = true;
  digitalWrite( TLBMSSAS, HIGH);
  delay(1);
  Serial3.write(startByte);
  Serial3.write(BMSADDRESS);
  Serial3.write(BATTERYUPDATE);
  Serial3.write(stopByte);
  Serial3.write(stopByte);
  delay(4);
  digitalWrite( TLBMSSAS, LOW);
}

bool variableIsAbout(int var1, int var2, int range) {
  return ((var1 < var2 + range) && (var1 < var2 - range));
}

void printSASdataXBEE() {

}
