/* 
 * File:   initSystems.h
 * Author: User
 *
 * Created on April 6, 2014, 4:45 PM
 */

#ifndef INITSYSTEMS_H
#define	INITSYSTEMS_H

#ifdef	__cplusplus
extern "C" {
#endif

void initRS485Direction(){
    L_T1=LISTEN;
    L_T2=LISTEN;
    delay_5ms();
}
void delay_5ms() {
    for (i = 0; i <= 50; i++) {
        DELAY_100uS;
    }
}
void delay_1s() {
    for (i = 0; i <= 200; i++) {
        delay_5ms();
    }
}



void oscillatorInit(){
    // Disable Watch Dog Timer
	RCONbits.SWDTEN=0;
        OSCCONbits.NOSC=2;
        OSCCONbits.OSWEN=1;
    // Wait for Clock switch to occur
    //while (OSCCONbits.COSC != 0b001);
    // Wait for PLL to lock
   // while (OSCCONbits.LOCK != 1);
    // Enable Watch Dog Timer
	//RCONbits.SWDTEN=1;

}
void initPins() {
    OSCCON=0x46;
    OSCCON=0x57;
    OSCCONbits.IOLOCK=0;
    AD1PCFGL = 0xFFFF; //configure the analog pins to digital

    TRISA = OUTPUT;
    TRISB = OUTPUT;
    TRISC = OUTPUT;
    NTC = INPUT;
    TRISBbits.TRISB0=OUTPUT;
    LATCbits.LATC3=1;   //pullup recieve line
    //PORTA = LOW;
    //PORTB = LOW;
    //PORTC = LOW;
    RELAY_ENABLE = HIGH;
    WatchDog=HIGH;    
    LED=HIGH;

    U1Rx_RPn = 19; //UART1 RX is RP19 for ECU bus
    RP20map = U1Tx_RPn; //UART1 TX isRP20 for ECU bus
//
//    U1Rx_RPn=12; //UART1RX is RP13 for USB
//    RP13map=U1Tx_RPn;  //UART1TX is RP12 for USB

    U2Rx_RPn = 23; //UART2RX is RP22 for Slave bus
    RP22map = U2Tx_RPn; //UART2TX is RP23 for Slave bus

    LATCbits.LATC7=1;
    LATCbits.LATC6=1;

    LATCbits.LATC4=1;
    LATCbits.LATC3=1;

    Interrupt0Pin = 24; //Interrupt 0 is Slave Interrupt line

    OSCCON=0x46;
    OSCCON=0x57;
    OSCCONbits.IOLOCK=1;


}

void initUARTS() {
    //----------------- UART1 config ----------------------   
    U1BRG = BRGVAL; // Baud Rate setting for 38400
    U1STAbits.UTXISEL0 = 0, U1STAbits.UTXISEL1 = 0; // Interrupt every character sent out (room for more)
    U1STAbits.URXISEL = 0; // Interrupt after one RX character is received
    U1STAbits.UTXINV=0;
    IFS0bits.U1TXIF = 0;	// Clear the Transmit Interrupt Flag
    //IEC0bits.U1TXIE = 1;	// Enable Transmit Interrupts
    IFS0bits.U1RXIF = 0;	// Clear the Recieve Interrupt Flag
    IEC0bits.U1RXIE = 1;	// Enable Recieve Interrupts

    U1MODEbits.UARTEN = 1; // Enable UART
    U1STAbits.UTXEN = 1; // Enable UART TX

    //----------------- UART2 config --------------------
    U2MODEbits.STSEL = 0; // 1-Stop bit
    U2MODEbits.PDSEL = 0; // No Parity, 8-Data bits
    U2MODEbits.ABAUD = 0; // Auto-Baud disabled
    U2MODEbits.BRGH = 0; // Standard-Speed mode
    U2BRG = BRGVAL; // Baud Rate setting for 38400
    U2STAbits.UTXISEL0 = 0, U2STAbits.UTXISEL1 = 0; // Interrupt every character sent out (room for more)
    U2STAbits.URXISEL = 0; // Interrupt after one RX character is received

    IFS1bits.U2TXIF = 0;	// Clear the Transmit Interrupt Flag
    //IEC1bits.U2TXIE = 1;	// Enable Transmit Interrupts
    IFS1bits.U2RXIF = 0;	// Clear the Recieve Interrupt Flag
    IEC1bits.U2RXIE = 1;	// Enable Recieve Interrupts
    
    U2MODEbits.UARTEN = 1; // Enable UART
    U2STAbits.UTXEN = 1; // Enable UART TX
}

#ifdef	__cplusplus
}
#endif

#endif	/* INITSYSTEMS_H */

